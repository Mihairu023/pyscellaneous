# pyscellaneous

## Some home projects that used to come in handy. Those include:

### * 7kanal_project: semi-automatic Google API-based news scraper and translator from Hebrew to Russian to avoid the open_new_translator_tab->close_new_translator_tab->goto1 (the translation limit has been exhausted so it won't work without a new account anyway).
#### Includes getting the news page using the requests library, translating it, formatting it the more convenient way (i.e. getting rid of redundant HTML-code requests lib yields), and saving it to the file. does emit intermittent files with the news written one by one (if uncomment the according lines) and holds a lot of fun with en/decoding inside. 
#### Lastly, it was intended to integrate the program with a Telegram bot but its main user chose to run it manually. 

### * USB_test: H2TestW-inspired scripts to preserve a dying (or merely glitchy) USB flash drive by:
#### 1. filling up a thumb drive with txt files with predefined data (e.g. "test" * 250 000), 
#### 2. trying to read them and checking if they consist of aforementioned data,
#### 3. adding numbers of files that had passed the check to the "succeeded" list,
#### 4. appending the "failed" list by numbers of those text files that turned out to be distorted (even by bit),
#### 5. filling the "epic failures" list up with files that were outright impossible to open,
#### 6. printing the stats of the tested thumb drive and finally
#### 7. deleting the good files to use the pen drive. bad files "catch" the bad blocks which makes the drive safer to use. 
### In comparison, H2TestW only fills the thumb drive up and checks if there are any bad blocks without specifying their location.

### * bea_soup_test: a script that utilized an open tutorial on the beautiful soup to download all the music files on a certain site simultaneously. (which did not include an option like "pay to download everything at once" so it's still legitimate). speaking of being able to learn and find open-source information on the required problems in a blink.

### There also was a project which was a cocktail of Flask, PsycoPG2, PostgreSQL, Pytest, and a tidbit of the Front-End Trio... but there was NDA too.
