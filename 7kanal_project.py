﻿# Нажми F5. Это запустит программу, бота и оставит файлы с бегунками
# там же, где и сама программа 7kanal_project.py.
# Файлы называются FLASH_ULTIMA.txt (сборник оригиналов)
# и FLASH_ULTIMA_RU.txt (сборник переводов, он-то тебе и нужен).

import telebot

print("Запускаем бота...")
TELEGRAM_BOT_KEY="welp probably there's no cake this impossible year'"
bot = telebot.TeleBot(TELEGRAM_BOT_KEY)

def SEVEN_MAIN():
    import requests
    import sys
    import fileinput
    import os
    SEVEN_PATH=os.path.realpath(__file__).split('7kanal_project.py')[0] #колдунство, дающее текущее местоположения скрипта, чтобы поудалять лишние файлы по соседству
    CRED_PATH=SEVEN_PATH+'My First Project-708350443202.json'
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = CRED_PATH
    #print(os.environ["GOOGLE_APPLICATION_CREDENTIALS"])

    from google.cloud import translate
    translate_client = translate.Client()

    LIST_OF_FLASHES=[]
    TEXT_ULTIMA=""" """
    TEXT_ULTIMA_RU=""" """

    HYPERLINK_LINK=""
    HYPERLINK_TEXT=""
    url='https://www.israelnationalnews.com/News/AllFlashes.aspx'
    r = requests.get(url)
    with open('test.html', 'wb') as output_file:
      output_file.write(r.text.encode('utf-8'))
      output_file.close()

    def WITH_AS_SAVER(FILE, TEXT):
        with open(FILE, 'wb') as OUTPUT_FLASH_FILE:
            OUTPUT_FLASH_FILE.write(TEXT.encode(sys.stdout.encoding, errors='ignore'))
            OUTPUT_FLASH_FILE.close()

    def SEVEN_TRANSLATE(TEXT, LANG_TARGET='ru', LANG_SOURCE='en'):
        TEXT=(translate_client.translate(TEXT, target_language=LANG_TARGET,source_language=LANG_SOURCE,model="nmt")['translatedText'])
        return TEXT

    def SEVEN_REPLACE(TEXT):
        TEXT=TEXT.replace('&#111;', 'o')
        TEXT=TEXT.replace('&quot;', '"')
        TEXT=TEXT.replace('</p><p>', '\n\n') #этим троим
        TEXT=TEXT.replace('<p>', '')         #порядок
        TEXT=TEXT.replace('</p>', '')        #не менять
        TEXT=TEXT.replace('&rsquo;', "'")
        TEXT=TEXT.replace('<em>', '')
        TEXT=TEXT.replace('</em>', '')
        TEXT=TEXT.replace('</div>', '')
        TEXT=TEXT.replace('&#39;', "'")
        TEXT=TEXT.replace('&amp;', '&')
        TEXT=TEXT.replace('<br />', '')
        TEXT=TEXT.replace('<br>', '')
        return TEXT.replace('7part7', '\n\n' + '-'*80 + '\n\n').replace('6drob6', '\n\n')

    def SEVEN_DELETE(FLAG_FLASH_PAGE=True, FLAG_FLASH_XX=True, FLAG_FLASH_RU_XX=True, FLAG_FLASH_ULTIMA=False, FLAG_FLASH_ULTIMA_RU=False):
        onlyfiles = [f for f in os.listdir(SEVEN_PATH) if os.path.isfile(os.path.join(SEVEN_PATH, f))] #дает список всех файлов по соседству со скриптом
        if FLAG_FLASH_PAGE:
            os.remove(SEVEN_PATH+'test.html')
            for x in range(40):
                os.remove(SEVEN_PATH+'FLASH_PAGE_%d.html' % x)
        if FLAG_FLASH_XX:
            for x in range(40):
                for THE_FILE in onlyfiles:
                    if THE_FILE.startswith('FLASH_'): #надеюсь, слегка ускорит поиск и позволит удалить только файлы FLASH_XX
                        if THE_FILE.startswith('FLASH_%d_' % x):
                            MARKED_FLASH_XX=THE_FILE
                            os.remove(SEVEN_PATH+MARKED_FLASH_XX)
                            onlyfiles.remove(MARKED_FLASH_XX)
        if FLAG_FLASH_RU_XX:
            for x in range(40):
                for THE_FILE in onlyfiles:
                    if THE_FILE.startswith('FLASH_RU_'): #надеюсь, слегка ускорит поиск и позволит удалить только файлы FLASH_XX_RU
                        if THE_FILE.startswith('FLASH_RU_%d_' % x):
                            MARKED_FLASH_RU_XX=THE_FILE
                            os.remove(SEVEN_PATH+MARKED_FLASH_RU_XX)
                            onlyfiles.remove(MARKED_FLASH_RU_XX)
        if FLAG_FLASH_ULTIMA:
            os.remove(SEVEN_PATH+'FLASH_ULTIMA.txt')
        if FLAG_FLASH_ULTIMA_RU:
            os.remove(SEVEN_PATH+'FLASH_ULTIMA_RU.txt')            

    PAGE_OF_FLASHES=open('test.html', 'rb')
    for x in PAGE_OF_FLASHES:
        if 'href=/News/Flash.aspx/' in x.decode('utf-8'):
            CRUTCH=(x.decode('utf-8').split(' ')[7][5:]) #рус - 6, 5
            LIST_OF_FLASHES.append('https://www.israelnationalnews.com' + CRUTCH) #надеюсь, даст строчку со ссылкой
    PAGE_OF_FLASHES.close()

    #print(LIST_OF_FLASHES) #только если что-то пойдет не так, выдает список ссылок на имеющиеся 40 бегунков
    assert(len(LIST_OF_FLASHES)==len(set(LIST_OF_FLASHES))) #проверяем, что повторяющихся новостей нет
    print("Страница с бегунками получена!")

    for FLASH_NUMBER, FLASH in enumerate(LIST_OF_FLASHES):
        FLASH_TEXT=""""""
        ACQUIRED=requests.get(FLASH)
        with open('FLASH_PAGE_%d.html' % FLASH_NUMBER, 'wb') as OUTPUT_FLASH_HTML_FILE:
            OUTPUT_FLASH_HTML_FILE.write(ACQUIRED.text.encode('utf-8'))
            OUTPUT_FLASH_HTML_FILE.close()
        MY_FLASH=open('FLASH_PAGE_%d.html' % FLASH_NUMBER, 'rb')
        for THE_ROW in MY_FLASH:
            if 'InfoPreGeoCountry=' in THE_ROW.decode('utf-8'):
                FLASH_HEADER_CRUTCH=(THE_ROW.decode('utf-8').split('<title>')[1]).split('</title>')[0].split(' - Israel National News')[0] #хватаем заголовки статей
                FLASH_TEXT+=FLASH_HEADER_CRUTCH+'\n\n'
            if '<div class="Right"><div class="b">' in THE_ROW.decode('utf-8'):
                FLASH_TIME_CRUTCH=(THE_ROW.decode('utf-8').split('<div class="Right"><div class="b">')[1].split(' </div><div class="a">')[0]) #берем время статьи
                FLASH_TEXT+=' (%s) 6drob6' % FLASH_TIME_CRUTCH
            if '<div class="Content" id="content">' in THE_ROW.decode('utf-8'):
                FLASH_CONTENT_CRUTCH=THE_ROW.decode('utf-8').split('<div class="Content" id="content">')[1] #и, наконец, саму статью
                FLASH_TEXT+=FLASH_CONTENT_CRUTCH
        MY_FLASH.close() #если все сломается, ты знаешь, кого винить
                
        for TEXT_ROW in FLASH_TEXT.split('\n\n'): #на случай превью к статье, а не бегунка
            if 'a href=' in TEXT_ROW:
                OH_FUCK_ANOTHER_CRUTCH=' 6drob6 Read more: https://www.israelnationalnews.com'+TEXT_ROW.split('a href=')[1].split('>Read more</a>')[0]
                FLASH_TEXT=FLASH_TEXT.split('<div style')[0]+OH_FUCK_ANOTHER_CRUTCH
            if '<a target="_blank"' in TEXT_ROW: #****** гиперссылки, жди случая протестить эту ветвь
                COORDS_CRUTCH=[]
                HYPERLINK_LINK=TEXT_ROW.split('<a target')[1].split('">')[0]
                HYPERLINK_TEXT=TEXT_ROW.split('">')[1].split('</a>')[0]
                COORDS_CRUTCH.append(TEXT_ROW.index('<a target="_blank" href="'))
                COORDS_CRUTCH.append(TEXT_ROW.index(HYPERLINK_TEXT)) #ЭТОГО ФИКСЬ
                print(FLASH_TEXT[0:COORDS_CRUTCH[0]])
                print(FLASH_TEXT[COORDS_CRUTCH[1]:])
                FLASH_TEXT=FLASH_TEXT[0:COORDS_CRUTCH[0]] + FLASH_TEXT[COORDS_CRUTCH[1]:] #СССУКА
                #print('<a target="_blank" href="'+HYPERLINK_LINK+'">'+HYPERLINK_TEXT+'</a>')
                print(COORDS_CRUTCH, FLASH_TEXT)
        if HYPERLINK_LINK==True: FLASH_TEXT+=' 6drob6 Гиперссылка на "'+HYPERLINK_TEXT+'": https://www.israelnationalnews.com'+HYPERLINK_LINK
        HYPERLINK_LINK=""
                    
        FLASH_TEXT+=' 6drob6 Оригинал: ' + LIST_OF_FLASHES[FLASH_NUMBER] + ' 7part7'
                    
        with open('FLASH_%d.txt' % FLASH_NUMBER, 'wb') as OUTPUT_READY_FLASH_FILE:
            FLASH_TEXT=SEVEN_REPLACE(FLASH_TEXT)
            TEXT_ULTIMA+=FLASH_TEXT 
            OUTPUT_READY_FLASH_FILE.write(FLASH_TEXT.encode(sys.stdout.encoding, errors='ignore'))
            OUTPUT_READY_FLASH_FILE.close()
            #os.rename - на свой страх и риск
            try:
                os.rename('FLASH_%d.txt' % FLASH_NUMBER, 'FLASH_%d_%s.txt' % (FLASH_NUMBER, FLASH_HEADER_CRUTCH.replace(' ', '_').replace(':', '_').replace('?', '_').replace('"', '')))
            except FileExistsError:
                os.remove('FLASH_%d.txt' % FLASH_NUMBER)

        with open('FLASH_RU_%d.txt' % FLASH_NUMBER, 'wb') as OUTPUT_READY_RU_FLASH_FILE:
            FLASHEY_TEXT=FLASH_TEXT.replace('--', '') #с одним он и уместные дефисы сожрет
            FLASHEY_TEXT=SEVEN_REPLACE(SEVEN_TRANSLATE(FLASHEY_TEXT))
            TEXT_ULTIMA_RU+=FLASHEY_TEXT + '\n\n' + '-'*80 + '\n\n'
            OUTPUT_READY_RU_FLASH_FILE.write(FLASHEY_TEXT.encode(sys.stdout.encoding, errors='ignore'))
            OUTPUT_READY_RU_FLASH_FILE.close()
            #os.rename - на свой страх и риск
            try:
                os.rename('FLASH_RU_%d.txt' % FLASH_NUMBER, 'FLASH_RU_%d_%s.txt' % (FLASH_NUMBER, SEVEN_TRANSLATE(FLASH_HEADER_CRUTCH).replace(' ', '_').replace(':', '_').replace('?', '_').replace('"', '')))
            except FileExistsError:
                os.remove('FLASH_RU_%d.txt' % FLASH_NUMBER)
                
        if FLASH_NUMBER%5==0 or FLASH_NUMBER==39:
            print("Готово %s/40 бегунков!" % str(int(FLASH_NUMBER)+1)) #у программистов все с нуля считается
            bot.send_message(432049112, "Готово %s/40 бегунков!" % str(int(FLASH_NUMBER)+1))

    #print(translate_client.translate(OUTPUT_PART_4, target_language='ru')) #отладка словаря

    with open('FLASH_ULTIMA.txt', 'wb') as OUTPUT_FINAL_FLASH_FILE:
        TEXT_ULTIMA=TEXT_ULTIMA.replace('\n', '823ПЕРЕВОДСТРОКИ823')
        OUTPUT_FINAL_FLASH_FILE.write(TEXT_ULTIMA.encode(sys.stdout.encoding, errors='ignore'))
    with fileinput.FileInput('FLASH_ULTIMA.txt', inplace=True) as file:
        for line in file:
            print(line.replace('823ПЕРЕВОДСТРОКИ823', chr(10)), end='')

    with open('FLASH_ULTIMA_RU.txt', 'w') as OUTPUT_FINAL_FLASH_FILE:
        OUTPUT_FINAL_FLASH_FILE.write(TEXT_ULTIMA_RU)
                
    SEVEN_DELETE()
    # Нажми F5. Это запустит программу, бота и оставит файлы с бегунками
    # там же, где и сама программа 7kanal_project.py.
    # Файлы называются FLASH_ULTIMA.txt (сборник оригиналов)
    # и FLASH_ULTIMA_RU.txt (сборник переводов, он-то тебе и нужен).

#отцовский - 432049112
#мой - 297853344

@bot.message_handler(content_types=['text'])
def get_text_messages(message):
    from os import stat
    bot.send_message(message.from_user.id, "Привет! Я Ченнеру Нана, помощник для тех, кто знаком с 7 каналом.")
    bot.send_message(message.from_user.id, "Пока умею только присылать переводы английских бегунков. В ответ на любой текст.")
    bot.send_message(message.from_user.id, "Сейчас пришлю порцию свежайших бегунков. Не теряйте решимость.")
    SEVEN_MAIN()
    bot.send_document(chat_id=message.chat.id, data=open('FLASH_ULTIMA_RU.txt', 'rb'))
    statinfo = stat('FLASH_ULTIMA_RU.txt')
    bot.send_message(message.from_user.id, "Не забывайте проверять размер файла. Если он не изменился с прошлого раза, значит, новых бегунков не было.")
    bot.send_message(message.from_user.id, "Размер последнего файла: %d. Запомните/запишите/держите это число рядом, чтобы иметь возможность сравнить файлы между собой." % statinfo.st_size)
bot.polling(none_stop=True, interval=0)
