from bs4 import BeautifulSoup
import requests, shutil
 
url = "http://maxpason.com/audio/"
max_list=[]
 
# Getting the webpage, creating a Response object.
response = requests.get(url)
 
# Extracting the source code of the page.
data = response.text
 
# Passing the source code to BeautifulSoup to create a BeautifulSoup object for it.
soup = BeautifulSoup(data, 'lxml')
 
# Extracting all the <a> tags into a list.
tags = soup.find_all('a')
 
# Extracting URLs from the attribute href in the <a> tags.
for tag in tags:
    #print(tag.get('href'))
    if tag.get('href').endswith('.mp3'):
        max_list.append(tag.get('href'))

dirfile = url
for a_song in max_list:
    filereq = requests.get(url+a_song,stream = True)
    with open(a_song,"wb") as receive:
        shutil.copyfileobj(filereq.raw,receive)
    del filereq
