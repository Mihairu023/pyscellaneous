import os
import schedule
import time

list_success=[]
list_failure=[]
list_epcfail=[]
def scheduled_search_of_bads():
    for x in range(100000): 
        if x<10: y="0000"+str(x)
        elif x>=10 and x<100: y="000"+str(x)
        elif x>=100 and x<1000: y="00"+str(x)
        elif x>=1000 and x<10000: y="0"+str(x)
        else: y=str(x)
        tt="""elif x>=1000 and x<10000: y="000"+str(x)
        elif x>=10000 and x<100000: y="00"+str(x)
        elif x>=100000 and x<1000000: y="0"+str(x)
        elif x>=1000000: y=str(x)"""
        try:
            file = open("e:\_bad\\bad"+y+".txt") #E:\
        #file = open("test.txt", "w")
            if file.read()==("test"*250000):
                print(y+": fine")
                list_success.append(y)
            else:
                print(y+": failure")
                list_failure.append(y)
            file.close()
        except FileNotFoundError: continue
        except UnicodeDecodeError:
            print(y+": failure")
            list_failure.append(y)
        except OSError:
            print(y+": EPIC FAILURE")
            list_epcfail.append(y)
            continue
    print(str(len(list_failure))+" bad files: "+str(list_failure))
    print(str(len(list_epcfail))+" bad files: "+str(list_epcfail))
    list_ttfails=list_failure+list_epcfail

    print("-"*60)
    ##print("This will free your space for good files. Proceed? (y\\n)")
    ##delete_answer=input()
    ##if delete_answer=="n" or delete_answer=="N": print("Fine.")
    ##if delete_answer=="y" or delete_answer=="Y":
    if 1:
        print("Freeing space, please stand by...")
        for good_file in os.listdir("e:\_bad\\"): #E:\
            if good_file.split("bad")[1].split(".txt")[0] not in list_ttfails:
                os.remove("e:\_bad\\"+good_file) #E:\
        print("Done. Over "+str(len(list_success))+" MBs more are available now.")

    ##else: print("Invalid answer. Space was not freed. Please restart the program.")
    print("-"*60)
    print("Remember: 1. DO NOT FORMAT THE FLASH DRIVE.")
    print("2. DO NOT MANUALLY DELETE THE \"_bad\" FOLDER OR FILES INSIDE OF IT UNDER ANY CIRCUMSTANCES.")
    print("Violating those rules may result in corruption of any files written on your device. That's too risky.")
    print("Please contact the developer via Telegram for further instructions in case of fairytale gone bad. He is kind.")
    print("@MichaelKolesnikov")


schedule.every().day.at("02:47").do(scheduled_search_of_bads)
print("Cycle enabled!")
# нужно иметь свой цикл для запуска планировщика с периодом в 1 секунду:
while True:
    schedule.run_pending()
    time.sleep(1)
